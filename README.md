This repository contains datasets related to social network analysis. To access a specific dataset, navigate to each folder in this repository and search for the desired subject.

Available datasets:

1.[Brazilian Symposium on Information Systems (SBSI)](https://gitlab.com/InfoKnow/SocialNetwork/datasets/-/tree/main/sbsi%20twenty%20years?ref_type=heads)