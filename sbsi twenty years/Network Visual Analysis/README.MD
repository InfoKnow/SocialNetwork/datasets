Here are the files for SBSI temporal network. Each file contains data about the network, as follow:

1-network
Pattern: **Node1_ID Node2_ID Timestamp**

2-metadata-color
Pattern: **Node_ID Label** (which define color node)

3-metadata-tooltip
Pattern: **nodeId Info1 Info2**

The three files MUST be inserted in order to work in LargeNetVis. Example: 

<img src="input.png" width="200">

The parameters used have default values. You can adjust the **Number of TimeSlices** parameter to divide the network however you want. Furthermore, you can configure the **Jaccard** parameter to have different results regarding the evolutionary taxonomy. Example:

<img src="parameters.png" width="200">